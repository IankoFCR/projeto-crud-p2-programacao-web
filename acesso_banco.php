<?php

  function exibir_alunos(){
    $alunos = [];
    $banco = fopen("banco.txt", "r");
    while(feof($banco) == false){
      $x["nome"] = fgets($banco);
      $x["matricula"] = fgets($banco);
      $x["nota1"] = fgets($banco);
      $x["nota2"] = fgets($banco);
      $x["media"] = fgets($banco);
      $alunos[] = $x;
    }
    fclose($banco);

    return $alunos;
  }

  function check_session(){
    $usuarios = [];
    $banco = fopen("usuarios.txt", "r");
    while(feof($banco) == false){
      $x["email"] = fgets($banco);
      $x["senha"] = fgets($banco);
      $usuarios[] = $x;
    }
    fclose($banco);

    return $usuarios;
  }
?>