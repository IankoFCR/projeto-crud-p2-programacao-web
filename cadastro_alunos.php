<?php 

session_start();

if(isset($_SESSION["logado"]) === false){
  header("Location: pagina_login.php");
}

if(isset($_GET["action"]) === true && $_GET["action"] === "logout"){
  session_destroy();
  header("Location: pagina_login.php");
}

$erro = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING);
    $matricula = filter_input(INPUT_POST, "matricula", FILTER_SANITIZE_STRING);
    $nota1 = filter_input(INPUT_POST, "nota1", FILTER_VALIDATE_FLOAT);
    $nota2 = filter_input(INPUT_POST, "nota2", FILTER_VALIDATE_FLOAT);
    
    if(!$nome){
      $erro = "Digite um Nome";

    }
    if(!$matricula){
      $erro = "Informe uma Matricula";
    }
    if(!$nota1 || !$nota2){
      $erro = "Notas invalidas";
    }

    else{
      $nome = $_POST["nome"];
      $matricula = $_POST["matricula"];
      $nota1 = $_POST["nota1"];
      $nota2 = $_POST["nota2"];

      $media = ($nota1 + $nota2)/2;

      $banco = fopen("banco.txt", "a");
      fwrite($banco, $nome."\n");
      fwrite($banco, $matricula."\n");
      fwrite($banco, $nota1."\n");
      fwrite($banco, $nota2."\n");
      fwrite($banco, $media."\n");
      fclose($banco);

      header("Location: tabela.php");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Cadastro alunos</title>

  <script>
  function confirmar()
  {
    alert("Aluno cadastrado!");
  }
  </script>

  <style>
    .error{
      color: red;
    }
  </style>
</head>
<body>
  <h1>Testando Validação</h1>
  <form action="cadastro_alunos.php" method="post">

    <p class="error"><?= $erro ?></p><br>
    
    <label for="nome">Nome: </label>
    <input type="text" name="nome" id="nome"><br><br>
    
    <label for="matricula">Matricula:</label>
    <input type="text" name="matricula" id="matricula"><br><br>

    <label for="nota1">Nota 1: </label>
    <input type="text" name="nota1" id="nota1"><br><br>

    <label for="nota2">Nota 2: </label>
    <input type="text" name="nota2" id="nota2"><br><br>

    <button onclick="confirmar()" >Cadastrar</button>
  </form>

  <p><a href="cadastro_alunos.php?action=logout"><button>Logout</button></a></p>

</body>
</html>