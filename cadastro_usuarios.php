<?php

$erro = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
  $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
  $senha = filter_input(INPUT_POST, "senha", FILTER_SANITIZE_STRING);

  if(!$email){
    $erro = "Informe um Email Válido!";
  }
  if(!$senha){
    $erro = "Informe uma Senha Válida!";
  }

  else{
    $email = $_POST["email"];
    $senha = $_POST["senha"];

    $banco = fopen("usuarios.txt", "a");
    fwrite($banco, $email."\n");
    fwrite($banco, $senha."\n");
    fclose($banco);

    header("Location: pagina_login.php");
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Cadastro De Usuarios</title>

  <script>
  function confirmar()
  {
    alert("Aluno cadastrado!");
  }
  </script>

  <style>
    .error{
      color: red;
    }
  </style>
</head>
<body>

  <h1>Crie sua Conta</h1>
  <form action="cadastro_usuarios.php" method="post">

    <p class="error"><?= $erro ?></p><br>

    <label for="email">Email: </label>  
    <input type="email" name="email" id="email"> <br><br>

    <label for="senha">Senha: </label>
    <input type="password" name="senha" id="senha"> <br><br>

    <button onclick="confirmar()">Cadastrar</button>
  </form>
</body>
</html>