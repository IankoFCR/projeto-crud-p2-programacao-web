<?php 

require_once "acesso_banco.php"; 

session_start();

if(isset($_SESSION["logado"]) === false){
  header("Location: pagina_login.php");
}

if(isset($_GET["action"]) === true && $_GET["action"] === "logout"){
  session_destroy();
  header("Location: pagina_login.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tabela de Alunos</title>
</head>
<body>
  <table border='1'>
    <tr>
      <th>Nome</th>
      <th>Matricula</th>
      <th>Nota 1</th>
      <th>Nota 2</th>
      <th>Média</th>
    </tr>
    <?php
        $alunos = exibir_alunos();
        foreach($alunos as $aluno){?>
            <tr>
              <td><?=$aluno["nome"]?></td>
              <td><?=$aluno["matricula"]?></td>
              <td><?=$aluno["nota1"]?></td>
              <td><?=$aluno["nota2"]?></td>
              <td><?=$aluno["media"]?></td>
            </tr>
        <?php } ?>
        <br>
        <a href="cadastro_alunos.php">Adicionar Aluno<a>
        <br><br>

  </table>
</body>
</html>