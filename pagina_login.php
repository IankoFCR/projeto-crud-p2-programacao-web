<?php require_once "acesso_banco.php"; ?>
<?php
    $erro = "";
    if($_SERVER["REQUEST_METHOD"] === "POST"){
        $usuarios = check_session();
        foreach($usuarios as $usuario){
            if($_POST["email"]."\n" === $usuario["email"] && $_POST["senha"]."\n" === $usuario["senha"]){
                session_start();
                $_SESSION["logado"] = true;
                $_SESSION["email"] = $_POST["email"];
                header("Location: tabela.php");
            }
            else{
              $erro = "Login ou senha Inválidos";
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Página de Login</title>
</head>
<body>
  <h1>Página de Login</h1>
  <p style="color:red;"><?=$erro?></p>
  <form action="pagina_login.php" method="POST">

    <label for="email">Email: </label>
    <input type="text" id="email" name="email"> <br><br>

    <label for="senha">Senha: </label>
    <input type="password" id="senha" name="senha"><br><br>
    
    <button>Logar</button>
  </form>
  <br>
  <a href="cadastro_usuarios.php">Cadastro<a>
</body>
</html>